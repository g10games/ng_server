import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../../../environments/environment';
const currentPath = environment.currentPath;
@Injectable({
  providedIn: 'root'
})
export class QuestionService {
  url: string = currentPath+"/api/questionSetDataAll";
  headers: any;
  options: any;
  constructor(private http: Http) {
    this.headers = new Headers(
      { 'Content-Type': 'application/json' }
    );
  }

  getDataQuestionSetAll() {
    this.options = new RequestOptions({
      headers: this.headers,
    });
    console.log(this.url)
    return this.http.get(this.url, this.options);
  }


  getQuestionSet(_id) {
    this.options = new RequestOptions({
      headers: this.headers,
      body: {
        "questionSet_id": Number(_id)
      }
    });
    return this.http.post(currentPath+'/api/questionSetDataById', '', this.options);

  }
  getNewQuestion(_id) {
    this.options = new RequestOptions({
      headers: this.headers,
      body: {
        "_id": Number(_id)
      }
    });
    return this.http.post(currentPath+'/api/getNewQuestion', '', this.options);
  }



}
