import { Injectable } from '@angular/core';
import { Meta } from '@angular/platform-browser';
import { environment } from '../../../../../../environments/environment';
const currentPath = environment.currentPath;
@Injectable({
  providedIn: 'root'
})
export class SeoService {

  constructor(private meta: Meta) { }

  generateTags(config) {
    // default values
    config = { 
      title: 'Test4Fun - Quiz สนุกๆสำหรับทุกคน อยากสนุกไปกับควิซคลิ๊กเลย!', 
      description: 'Test4Fun - Quiz สนุกๆสำหรับทุกคน อยากสนุกไปกับควิซคลิ๊กเลย!', 
      image: currentPath+ '/upload/canvas/coverFacebook.jpg',
      url: '',
      ...config
    }
    this.meta.updateTag({ property: 'og:type', content: 'article' });
    this.meta.updateTag({ property: 'og:site_name', content: 'Test4Fun' });
    this.meta.updateTag({ property: 'og:title', content: config.title });
    this.meta.updateTag({ property: 'og:description', content: config.description });
    this.meta.updateTag({ property: 'og:image', content: config.image });
    this.meta.updateTag({ property: 'og:url', content: config.url });
  }
}
