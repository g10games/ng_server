import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../../../environments/environment';
const currentPath = environment.currentPath;
@Injectable({
  providedIn: 'root'
})
export class QuizService {
  headers: any;
  options: any;
  constructor(private http: Http) {
    this.headers = new Headers(
      { 'Content-Type': 'application/json' }
    );
  }

  createDataUser(idFacebook, nameFacebook, emailFacebook) {
    this.options = new RequestOptions({
      headers: this.headers,
      body: {
        "idFacebook": idFacebook,
        "nameFacebook": nameFacebook,
        "emailFacebook": emailFacebook
      }
    });
    return this.http.post(currentPath+'/api/userRecive', '', this.options);
  }

  readDataQuestion(_id: Number) {
    this.options = new RequestOptions({

      headers: this.headers,
      body: {
        "questionSet_id": _id
      }

    });
    console.log(this.options);
    return this.http.post(currentPath+'/api/questionData', '', this.options);
  }

  checkAnswer(_id, choice, answerValue,questionRule) {
    this.options = new RequestOptions({
      headers: this.headers,
      body: {
        "questionSet_id": Number(_id),
        "answer": choice,
        "answerValue":answerValue,
        "questionRule" : questionRule
      }
    });
    return this.http.post(currentPath+'/api/checkAnswer', '', this.options);
  }

  readDataUser(idFacebook) {
    this.options = new RequestOptions({
      headers: this.headers,
      body: {
        "idFacebook": idFacebook
      }
    });
    return this.http.post(currentPath+'/api/userData', '', this.options);
  }

  createGetData(questionSet_id, user_id) {
    this.options = new RequestOptions({
      headers: this.headers,
      body: {
        "questionSet_id": questionSet_id,
        "user_id": user_id
      }
    });
    return this.http.post(currentPath+'/api/getDataCreate', '', this.options);
  }


  createUserAnswer(user_id, questionSet_id,userAnswer,answer_result,result_img) {
    this.options = new RequestOptions({
      headers: this.headers,
      body: {
        "questionSet_id": questionSet_id,
        "user_id": user_id,
        "userAnswer" : userAnswer ,
        "answer_result" : answer_result,
        "result_img" : result_img

      }
    });
    console.log("User Answer Created",this.options)
    return this.http.post(currentPath+'/api/createUserAnswer', '', this.options);
  }

}
