import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HeaderComponent } from './component/header/header.component';
import { FooterComponent } from './component/footer/footer.component';

const COMPONENTS = [
  HeaderComponent,
  FooterComponent
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [CommonModule, NgbModule],
  exports: [CommonModule, ...COMPONENTS],
})
export class themeModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: themeModule,
    };
  }
}
