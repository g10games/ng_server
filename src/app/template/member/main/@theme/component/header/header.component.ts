import { Component, OnInit } from '@angular/core';
import { Router, Event } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  facebookToken: any;
  facebookFirtsName: string = "";
  facebookPicture: string = "";
  facebookLastName: string = "";
  statusHeader: number = 0;
  getUrl: string = "";
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.facebookToken = localStorage.getItem('facebookToken');

    if (this.facebookToken != null) {
      this.statusHeader = 1;
      this.facebookFirtsName = localStorage.getItem('facebookFirtsName');
      this.facebookLastName = localStorage.getItem('facebookLastName');
      this.facebookPicture = localStorage.getItem('facebookPicture');
    }
    else {
      this.statusHeader = 0;
      this.facebookPicture = './../../../../../../../assets/imgs/facebook.jpg';
    }
  }

  backToQuestion() {
    // window.location.reload();
    localStorage.setItem('statusReload', '1');
    this.router.navigate(['/', '']);
  }

  facebookLogout() {
    this.getUrl = this.router.url;
    if (this.getUrl == "/") {
      localStorage.clear();
      window.location.reload();
    }
    else {
      localStorage.clear();
      this.router.navigate(['/', '']).then(() => {
        window.location.reload();
      })
    }
  }

}
