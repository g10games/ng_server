import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Service
import { QuestionService } from '../../@core/service/question.service';
import { SeoService } from '../../@core/service/seo.service';
import { environment } from '../../../../../../environments/environment';
const getPath = environment.currentPath;  //get environment path local/test4fun
@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {
	currentPath :any;
  getData: any;
  numData: number;
  statusSpinner: number = 0;
  statusReload: string = '';
  constructor(
    private router: Router,
    private questionService: QuestionService,
    // private meta: Meta
    private seo: SeoService
  ) {
    // window.location.reload();
    this.statusReload = localStorage.getItem('statusReload');
    if (this.statusReload == '1') {
      localStorage.removeItem('statusReload');
      window.location.reload();
    }
    this.getDataQuestionSet();
  }

  ngOnInit() {
    console.log("test")
    this.seo.generateTags({

      title: 'Test4Fun - Quiz สนุกๆสำหรับทุกคน อยากสนุกไปกับควิซคลิ๊กเลย!', 
      description: 'Test4Fun - Quiz สนุกๆสำหรับทุกคน อยากสนุกไปกับควิซคลิ๊กเลย!', 
      image: getPath + '/upload/canvas/coverFacebook.jpg',
      url: getPath+'/'
    })
  }

  gotoQuiz(_id: any, imgCover) {
    // localStorage.setItem('questionSet_id', _id);
    // localStorage.setItem('questionSetName', questionSetName);	
    localStorage.setItem('imgCover', imgCover);
    // this.router.navigate(['/', 'quiz']);
    this.router.navigate(['quiz/', _id]);

  }

  getDataQuestionSet() {
    this.statusSpinner = 0;
    this.questionService.getDataQuestionSetAll().subscribe((res) => {
    this.currentPath = getPath;
      this.getData = JSON.parse(res['_body']);
      this.statusSpinner = 1;
      this.numData = this.getData.length;
    });
  }

}
