import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuestionComponent } from './question/question.component';
//import { Somewhere } from './somewhere/somewhere.component';
import { PageComponent } from './page.component';
import { PolicyComponent } from './policy/policy.component';


const routes: Routes = [
  {
    path: '',
    component: PageComponent,
    children: [
      {
        path: '',
        component: QuestionComponent,
      },
      {
        path: 'quiz',
        redirectTo: ''
      },
      {
        path: 'quiz',
        loadChildren: './quiz/quiz.module#QuizModule',
      },
      {
        path: 'policy',
        component: PolicyComponent,
      }
      // {
      //   path: 'somewhere',
      //   component: Somewhere,
      // }
     ]

  },
  { path: '', redirectTo: '', pathMatch: 'full' },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PageRoutingModule { }
