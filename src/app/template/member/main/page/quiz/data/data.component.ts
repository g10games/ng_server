//<!-- Header หน้ากดลอคอินเฟสบุค-->
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FacebookService, InitParams, LoginOptions , UIParams, UIResponse } from 'ngx-facebook';
import { environment } from '../../../../../../../environments/environment';
const currentPath = environment.currentPath;
const app = environment.app
// Service
import { QuestionService } from '../../../@core/service/question.service';
import { QuizService } from '../../../@core/service/quiz.service';
import { SeoService } from '../../../@core/service/seo.service';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss']
})
export class DataComponent implements OnInit {
  questionSet_id: any;
  getData: any;
  questionSetName: string = "";
  imgCover: string = "";
  myObj: any;
  facebookToken: any;
  facebookFirtsName: string = "";
  facebookLastName: string = "";
  facebookPicture: string = "";
  buttonText: string = "";
  shareText : string = "";
  statusSpinner: number = 0;
  status: number = 0;
  type: String = "";
  message: String = "";
  getUrl: any;
  private sub: any;
  constructor(
    private router: Router,
    private routes: ActivatedRoute,
    private fb: FacebookService,
    private questionService: QuestionService,
    private quizService: QuizService,
    // private metaService: Meta
    private seo: SeoService
  ) {
    this.sub = this.routes.params.subscribe(params => {
      this.router.events.subscribe(() => {
        this.getUrl = this.router.url.split("/")[2];
        if (this.getUrl == "undefined") {
          localStorage.removeItem('statusPageStart');
          this.router.navigate(['/', '']);
        }
      });
      this.questionSet_id = + params['id'];
    });

    this.facebookToken = localStorage.getItem('facebookToken');

    if (this.facebookToken != null) {
      this.facebookFirtsName = localStorage.getItem('facebookFirtsName');
      this.buttonText = "Proceed With Name";
    }
    else {
      this.buttonText = "Login With Facebook";
      this.facebookFirtsName = " ";
    }
    this.readData();
  }

  ngOnInit() {
    const initParams: InitParams = {
      //appId: '2263395557242765',
      appId: app.id,
      version: app.version
    };

    this.fb.init(initParams);
  }

  readData() {
    this.questionService.getQuestionSet(this.questionSet_id).subscribe((res) => {
    console.log(this.questionSet_id);
    localStorage.setItem('current_question_id',this.questionSet_id);
      var data = JSON.parse(res['_body']);
      console.log("THIS IS DATA",data);
      if (data == "") {

        this.status = 1;
        this.type = "danger";
        this.message = "ไม่สามารถค้นหาแบบทดสอบได้555"
        setTimeout(() => {
          this.status = 0;
          this.router.navigate(['/', '']);
        }, 2500);
      }
      else {
        this.getData = data
        this.questionSetName = data[0].questionSetName;
        this.imgCover = currentPath+"/upload/questionSet/" + data[0].imgCover;
        this.statusSpinner = 1;

        this.seo.generateTags({
          title: this.questionSetName,
          description: this.questionSetName,
          image: currentPath+"/upload/questionSet/" + data[0].imgCover,
          url: currentPath+'/quiz/' + this.questionSet_id
        })
      }
    })
  }

  shareWithOpenGraphActions() {
  // console.log(this.nameImg);
  console.log("SHARE")

  console.log("status share = 1")
  const params: UIParams = {
  method: 'share_open_graph',
  action_type: 'og.shares',
  action_properties: JSON.stringify({
    object: {
      //'og:url': 'https://dev.test4fun.me/quiz/' + this.questionSet_id,
      //'og:url': 'http://localhost/quiz/' + this.questionSet_id,
      'og:title': this.getData[0].questionSetName,
      'og:description': this.getData[0].questionSetDescription,
      //'og:image': this.getData[0].imgCover,
      'og:image': "http://lh3.googleusercontent.com/tAkB3XZlsLPHVNW6OMRyXMT3rzB_Nz-mSztQtrIIoIdGLI6kt_94adxRah9eWu6BAaeAZ3qEw7jcCE_B_KuZ=s0",
      //'og:image': 'http://localhost/upload/canvas/' + this.nameImg,
      'og:image:width': '1200',
      'og:image:height': '630',
        }
      })
    };
    this.fb.ui(params)
      .then((res: UIResponse) => window.location.reload())
      .catch((e: any) => console.log());
    console.log(params)
  
    
  }
  Facebook() {
    if (this.facebookToken != null) {
    //if (this.facebookToken == null) {
      localStorage.setItem('statusPageStart', this.questionSet_id);
      this.gotoQuizStart();
    }
    else {
      const loginOptions: LoginOptions = {
        enable_profile_selector: true,
        return_scopes: true,
        scope: 'public_profile, email'
      }
      this.fb.login(loginOptions).then((res) => {
        if (res['status'] == "connected") {
          this.fb.api(
            '/me',
            "get",
            {
              locale: 'en_US', fields: 'id, first_name, last_name, picture, email'
            }
          ).then((response) => {
            localStorage.setItem('facebookToken', res.authResponse['accessToken']);
            localStorage.setItem('idFacebook', response.id);
            localStorage.setItem('facebookFirtsName', response.first_name);
            localStorage.setItem('facebookLastName', response.last_name);
            localStorage.setItem('facebookPicture', response.picture.data.url);
            localStorage.setItem('facebookEmail', response.email);
            // localStorage.setItem('statusPageStart', this.questionSet_id);
            this.quizService.createDataUser(response.id, (response.first_name + " " + response.last_name), response.email).subscribe((res) => {
              // this.gotoQuizStart();
              window.location.reload();
            });
          })
        }
      });
    }
  }

  gotoQuizStart() {
    this.router.navigate(['quiz/' + this.questionSet_id + '', 'start']);
  }

}