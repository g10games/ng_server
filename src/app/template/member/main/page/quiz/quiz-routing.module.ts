import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QuizComponent } from './quiz.component';
import { DataComponent } from './data/data.component';
import { StartComponent } from './start/start.component';
import { ResultComponent } from './result/result.component';

const routes: Routes = [
  {
    path: '',
    component: QuizComponent,
    children: [
      {
        path: ':id',
        component: DataComponent,
      }, {
        path: ':id/start',
        component: StartComponent,
      }, {
        path: ':id/result',
        component: ResultComponent,
      }
    ]
  },
  { path: '', redirectTo: '', pathMatch: 'full' },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuizRoutingModule { }
