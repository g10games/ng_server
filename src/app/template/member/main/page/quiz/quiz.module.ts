import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuizRoutingModule } from './quiz-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { themeModule } from './../../../../../template/member/main/@theme/theme.module';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { FacebookModule } from 'ngx-facebook';
// import { AppComponent } from './../../../../../app.component';

// Service
import { QuizService } from './../../@core/service/quiz.service'

import { QuizComponent } from './quiz.component';
import { DataComponent } from './data/data.component';
import { StartComponent } from './start/start.component';
import { ResultComponent } from './result/result.component';

const PAGES_COMPONENTS = [
  QuizComponent,
  DataComponent,
  ResultComponent,
  StartComponent
];

@NgModule({
  declarations: [PAGES_COMPONENTS],
  imports: [
    CommonModule,
    QuizRoutingModule,
    NgbModule,
    themeModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    FacebookModule.forRoot(),
  ],
  providers: [QuizService],
  // bootstrap: [AppComponent]
})
export class QuizModule { }
