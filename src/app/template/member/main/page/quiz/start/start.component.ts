import { Component, OnInit } from '@angular/core';
import { Router, Event, ActivatedRoute } from '@angular/router';

// Service
import { QuizService } from './../../../@core/service/quiz.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})
export class StartComponent implements OnInit {
  hasLayout : Boolean;
  indexAns : any;
  questionRule : any ;
  questionSet_id: any;
  getData: any;
  question: string = "";
  choice: any;
  numObj: number = 0; // นับจำนวนทั้งหมดของข้อมูลที่มีอยู่ในฐานข้อมูล
  numData: number = 0; // นับจำนวนของข้อมูลที่ใช้เเสดงผล
  numLoop: number = 1; // นับจำนวนทั้งหมดของข้อมูลที่ยังมีอยู่
  setChoice: any = [];
  getChoice: any = [];
  addValue : number = 0;
  choiceValue : any = [];
  statusSpinner: number = 0;
  statusPageStart: string = "";
  getUrl: string = "";
  private sub: any;
  // result: any = [];
  constructor(
    private router: Router,
    private routes: ActivatedRoute,  
    private quizService: QuizService
  ) {
    this.sub = this.routes.params.subscribe(params => {
      this.questionSet_id = +params['id'];
      // alert( this.questionSet_id)
    });

    this.statusPageStart = localStorage.getItem('statusPageStart');
    this.router.events.subscribe((event: Event) => {
      this.getUrl = this.router.url.split("/")[2];
      if (this.statusPageStart != this.getUrl) {
        localStorage.removeItem('statusPageStart');
        this.router.navigate(['quiz/' + this.getUrl]);
      }
    });
    localStorage.removeItem('answerRule');
    localStorage.removeItem('answerValue');
    localStorage.removeItem('getChoice');
    this.readDataQuestion(); //Read Data Question in Database
  }

  ngOnInit() {
  }

  readDataQuestion() {
  	this.hasLayout = true ;
    this.statusSpinner = 0;
    this.quizService.readDataQuestion(this.questionSet_id).subscribe((res) => {
      this.getData = JSON.parse(res['_body']);

      console.log("get rule = ",this.getData);
      try{
		console.log("get layout = ",this.getData[0].layout_design);
      	console.log("get layout rect position = ",this.getData[0].layout_design.rect.position);
      	console.log("get layout font = ",this.getData[0].layout_design.text);
      }
      catch(err) {
      	this.hasLayout = false ;
  		console.log("no layout")
		}
      //console.log("get layout = ",this.getData[0].layout_design);
      //console.log("get layout rect position = ",this.getData[0].layout_design.rect.position);
      //console.log("get layout font = ",this.getData[0].layout_design.text);
      this.questionRule = this.getData[this.numData].rule.name
      //this.question = this.getData[this.numData]['questionDesc'][0]['question'];
      this.question = this.getData[this.numData]['question_description'][0]['question'];
      //console.log("666");
      console.log(this.getData[this.numData]['questionSetName']);
      localStorage.setItem('questionSetname', this.getData[this.numData]['questionSetName']);
      this.choice = this.getData[this.numData]['question_description'][0]['choice'];
      this.choice = Object.values(this.choice);
      this.numObj = this.getData[this.numData]["question_description"].length;
      //console.log(this.getData);
      //console.log(this.numObj);  
      //console.log("123");
      this.statusSpinner = 1;
      //localStorage.setItem('questionSetname', this.question);
      //localStorage.setItem('questionSetname', "asdasd");
      //localStorage.setItem('current_q_id', "1");
    })
  }

  numQuestion(numData, text,indexAns) {
    console.log("GET DATA",this.getData)
    console.log("GET DATA",this.indexAns)
    console.log("ANSWER = ",this.getData[0]["question_description"][numData-1]["choice"][indexAns]["value"]);
    this.setChoice.push(text);
    this.choiceValue.push(this.getData[0]["question_description"][numData-1]["choice"][indexAns]["value"]);
    console.log("this.addValue1",this.addValue);
    console.log("VALUE",this.getData[0]["question_description"][numData-1]["choice"][indexAns]["value"])
    this.addValue += parseInt(this.getData[0]["question_description"][numData-1]["choice"][indexAns]["value"]);

    console.log("this.addValue2",this.addValue);
    console.log("loop");
    console.log(this.numLoop);

    if (this.numLoop == this.numObj) {

      // alert(this.setChoice)
      localStorage.setItem('getChoice', this.setChoice);
      console.log('this.addValue ',this.addValue);
      

      localStorage.setItem('choiceValue',this.choiceValue);
      localStorage.setItem('questionRule',this.questionRule);
      localStorage.setItem('answerValue',this.addValue.toString());

    	
      this.router.navigate(['quiz/' + this.questionSet_id + '', 'result']);
      // this
    } else {
      this.numLoop++;
      //this.question = this.getData[numData]['data']['question'][0]['question'];
      //this.choice = this.getData[this.numData]['data']['question'][0];
      console.log('this.getData ',this.getData);
      console.log('this.getData[this.numData] ',this.getData[0]);

      console.log('this.numLoop ',this.numLoop);
      this.question = this.getData[0]['question_description'][this.numLoop-1]['question'];
      console.log('this.question  ',this.question)
      this.choice = this.getData[0]['question_description'][this.numLoop-1]['choice'];
      this.choice = Object.values(this.choice);
      
    }
  }



  gotoResult(i: number) {
    // var text = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
    var text = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    this.numData++;
    this.numQuestion(this.numData, text[i], i);
  }


}
