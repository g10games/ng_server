import { Component, OnInit, ViewChild, Input, ElementRef, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FacebookService, InitParams, UIParams, UIResponse } from 'ngx-facebook';
import { Http, RequestOptions } from '@angular/http';

// Service
import { QuizService } from './../../../@core/service/quiz.service';
import { QuestionService } from '../../../@core/service/question.service';
import { error } from '@angular/compiler/src/util';
import { environment } from '../../../../../../../environments/environment';
const currentPath = environment.currentPath;
const app = environment.app
@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent {
  @ViewChild('canvas') canvas: ElementRef;
  resultCanvas : any;
  /////////////LAYOUT/////////////////
  currentPath: any ;
  textSize : any;
  textColor : any;
  textPosition : any;
  rectPosition : any ;
  ////////////////////////////////////////
  userName : any ;
  questionRule :any ;
  answerValue : any;
  current_q_id: any ;
  questionSet_id: any;
  getChoice: any;
  choice: string = "";
  getImg: string = "";
  getImgCover: string = "";
  getAnswer: string = "";
  getContent: string = "";
  question: string = "";
  statusSpinner: Number = 1;
  getQData : any ;
  getData: any;
  numData: number;
  idFacebook: any;
  getImageCanvas: string = "";
  headers: any;
  options: any;
  statusShare: Number = 0;
  nameImg: string = "";
  private sub: any;
  private ctx: CanvasRenderingContext2D;
  constructor(
    private router: Router,
    private routes: ActivatedRoute,
    private fb: FacebookService,
    private quizService: QuizService,
    private questionService: QuestionService,
    private http: Http
  ) {
    this.currentPath = currentPath;
    this.sub = this.routes.params.subscribe(params => {
      this.questionSet_id = +params['id'];
    });
    this.getChoice = localStorage.getItem('getChoice');
    this.questionRule = localStorage.getItem('questionRule');
    //this.answerValue = localStorage.getItem('answerValue');
    this.question = localStorage.getItem('questionSetname');
    this.idFacebook = localStorage.getItem('idFacebook');
    this.userName = localStorage.getItem('facebookFirtsName');
    // alert(this.idFacebook)
    if (this.getChoice == null) {
      this.router.navigate(['/', '']);
    }
    else {
      for (var i = 0; i < this.getChoice.length; i++) {
        if (this.getChoice[i] != ",") {
          this.choice = this.choice + (this.getChoice[i]);
        }
      }
      this.readUser(this.idFacebook);
    }
    // this.shareWithOpenGraphActions();
  }


  ngOnInit() {
    const initParams: InitParams = {
      //appId: '2263395557242765',
      appId: app.id,
      version: app.version
    };
    this.fb.init(initParams);

  }
  readUser(id) {
    this.quizService.readDataUser(id).subscribe((res) => {
      var data = JSON.parse(res['_body']);
      // this.quizService.createGetData(this.questionSet_id, data[0]._id).subscribe((res) => { })
    });
    this.checkAnswer();
  }

  checkAnswer() {
    console.log('result file')
    this.statusSpinner = 0;
    this.current_q_id = localStorage.getItem('current_question_id');
    this.answerValue = localStorage.getItem('answerValue');
    this.getLayout(this.current_q_id);
    console.log("get question set id",this.current_q_id)

    this.quizService.checkAnswer(this.questionSet_id, this.choice, this.answerValue,this.questionRule).subscribe((res) => {
      var data = JSON.parse(res['_body']);
      console.log("-- DATA -- ", data);
      console.log("-- ENVIRONMENT -- ", environment.currentPath);
      this.currentPath = environment.currentPath ;
      if (res.status = 200) {
        console.log("location ",location.hostname)

        if (location.hostname == "localhost"){
          console.log("this is local server")
          
        }
         	
        console.log('START')
        this.getImg = this.currentPath+"/upload/answerSet/" + data[0].img;
        //this.getImg = "https://apiv2.test4fun.me/upload/questionSet/19b1b8a93.jpg";
        console.log(data[0].img)
        console.log(this.getImg)


        //this.getImg = "https://apiv2.test4fun.me/upload/answerSet/" + data[0].img;
        // this.getImg = "http://localhost/upload/answerSet/" + data[0].img;
        
        //this.nameImg = data[0].img;
        this.nameImg = data[0].img;
        console.log(this.nameImg)
        this.getAnswer = data[0].answer;
        this.getContent = data[0].content;
        console.log("Get content",this.getContent);
        const source = new Image();
        console.log("get img",this.getImg)
        const canvasEl: HTMLCanvasElement = this.canvas.nativeElement;
        this.ctx = canvasEl.getContext('2d');
        source.crossOrigin = 'Anonymous';
        console.log("1")
        // window.location. = () => {
          source.onload = () => {
          //console.log('before this 1')
          this.ctx.drawImage(source, 0, 0, 1200, 630);
          this.createBoxForshowDescription(this.ctx);
          console.log("2")
          this.getImageCanvas = canvasEl.toDataURL();
          console.log("get img",this.getImg)
          console.log("get image canvas",this.getImageCanvas) //SHARED IMAGE
          this.headers = new Headers(
            { 'Content-Type': 'application/json' }
          );
          this.options = new RequestOptions({
            headers: this.headers,
            body: {
              data: {
                "idFacebook": this.idFacebook,
                "nameImg": (data[0].img),
                "imageCanvas": this.getImageCanvas
              }
            }
          });
            //this.http.post("https://apiv2.test4fun.me/api/canvasPicture", '', this.options).subscribe((res) => {

            this.http.post(this.currentPath+"/api/canvasPicture", '', this.options).subscribe((res) => {
            if (res.status = 200) {
              this.statusShare = 1;
            }
            else {
              var error = "error"
              throw error;
            }
          });
          this.statusSpinner = 1;
          this.getDataQuestionSet();
          
         // console.log("QUESTION SET ID-----",this.current_q_id)
          //console.log("DATA-----",data[0])
          //console.log("ANSWER CONTENT -----",data[0]["content"])
         // console.log("this.current_q_id",this.current_q_id)
          this.createUserAnswer(this.idFacebook,this.current_q_id,data[0]["answer"],data[0]["content"],data[0]["img"]);
          // console.log(this.images)
         }
        //source.src = "https://apiv2.test4fun.me/upload/answerSet/" + data[0].img;
        //console.log('this 2')

        source.src = this.currentPath+"/upload/answerSet/" + data[0].img;
        //source.src = "https://apiv2.test4fun.me/upload/questionSet/19b1b8a93.jpg";

        // }
      }
    });
  }

  getLayout(qid) {
    this.quizService.readDataQuestion(qid).subscribe((res) => {
      this.getQData = JSON.parse(res['_body']);
      this.statusSpinner = 1;
      this.numData = this.getQData.length;
      // alert(this.getData)
      //console.log(".Q data -- ", this.getQData)
    });

  }  




  getDataQuestionSet() {
    this.questionService.getDataQuestionSetAll().subscribe((res) => {
      this.getData = JSON.parse(res['_body']);
      this.statusSpinner = 1;
      this.numData = this.getData.length;
      // alert(this.getData)
      //console.log(".getData -- ", this.getData)
    });

  }

  createUserAnswer(user_id, questionSet_id,userAnswer,answer_result,answer_img){
    this.quizService.createUserAnswer(user_id, questionSet_id,userAnswer,answer_result,answer_img).subscribe((res) => {
      //this.getData = JSON.parse(res['_body']);
      //this.statusSpinner = 1;
      //8this.numData = this.getData.length;
      // alert(this.getData)
      //console.log("COMPLETED -- ", this.getData)
    });
  }






  gotoQuiz(_id: any) {
    this.router.navigate(['quiz/' + _id]).then(() => {
      window.location.reload();
    })
  }

  shareWithOpenGraphActions() {
    // console.log(this.nameImg);
    console.log("SHARE")
    if (this.statusShare == 1) {
      console.log("status share = 1")
      const params: UIParams = {
        method: 'share_open_graph',
        action_type: 'og.shares',
        action_properties: JSON.stringify({
          object: {
            //'og:url': 'https://dev.test4fun.me/quiz/' + this.questionSet_id,
            //'og:url': 'http://localhost/quiz/' + this.questionSet_id,
            //'og:url': "https://www.sanook.com/news/7763058/",
            //'og:url': 'https://th.vonvon.me/quiz/97',
            'og:title': this.question,
            'og:description': 'here custom description',
            //'og:image': this.currentPath+'/upload/canvas/' + this.nameImg,
            'og:image': "http://lh3.googleusercontent.com/tAkB3XZlsLPHVNW6OMRyXMT3rzB_Nz-mSztQtrIIoIdGLI6kt_94adxRah9eWu6BAaeAZ3qEw7jcCE_B_KuZ=s0",
            //'og:image': 'http://localhost/upload/canvas/' + this.nameImg,
            'og:image:width': '1200',
            'og:image:height': '630',
          }
        })
      };
      this.fb.ui(params)
        .then((res: UIResponse) => window.location.reload())
        .catch((e: any) => console.log());
      console.log(params)
    }
    
  }

  // Canvas
  createBoxForshowDescription(ctx) {

    try{ //new structure with layout field in database
      console.log("TRY")
      var text = this.getAnswer+" "+ this.getContent ; // TEXT IN CANVAS
      ctx.globalAlpha = 0.85;
      ctx.fillStyle = "black";
      ctx.fillRect(this.getQData[0].layout_design.rect.position[0],this.getQData[0].layout_design.rect.position[1], this.getQData[0].layout_design.rect.position[2], this.getQData[0].layout_design.rect.position[3]); //TOP
      ctx.font = this.getQData[0].layout_design.text.font;
      ctx.fillStyle = this.getQData[0].layout_design.text.style;
      this.wrapText(ctx, text, this.getQData[0].layout_design.text.position[0], this.getQData[0].layout_design.text.position[1], this.getQData[0].layout_design.text.position[2], this.getQData[0].layout_design.text.position[3]) //TOP
       }


    catch(err){ // old structure without layout field
      console.log("CATCH")
      var text = this.getAnswer+" "+ this.getContent ; // TEXT IN CANVAS
      ctx.globalAlpha = 0.85;
      ctx.fillStyle = "black";
      ctx.fillRect(0, 480, 1300, 150); // BOTTOM
      ctx.font = "24px Arial";
      ctx.fillStyle = "white";
      this.wrapText(ctx, text, 20, 515, 1000+500, 20) // BOTTOM
      

      }


  }

  wrapText(ctx, text, x, y, maxWidth, lineHeight) { //insert text in canvas
    var words = text.split(' ');
    var line = '';
    for (var n = 0; n < words.length; n++) {
      var testLine = line + words[n] + ' ';
      var metrics = ctx.measureText(testLine);
      var testWidth = metrics.width;
      if (testWidth > maxWidth && n > 0) {
        ctx.fillText(line, x, y);
        line = words[n] + ' ';
        y += lineHeight;
      }
      else {
        line = testLine;
      }
    }
    ctx.fillText(line, x, y);
  }


}
