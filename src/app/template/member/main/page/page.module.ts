import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageRoutingModule } from './page-routing.module';
import { themeModule } from './../../../../template/member/main/@theme/theme.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule } from "@angular/forms";
import { FacebookModule } from 'ngx-facebook';
// import { AppComponent } from './../../../../app.component';

// Service
import { QuestionService } from '../@core/service/question.service'


// Component
import { PageComponent } from './page.component';
import { QuestionComponent } from './question/question.component';
import { PolicyComponent } from './policy/policy.component';
//import { Somewhere } from './somewhere/somewhere.component';
const PAGES_COMPONENTS = [
  PageComponent,
  QuestionComponent,
  PolicyComponent
  //Somewhere

];

@NgModule({
  declarations: [PAGES_COMPONENTS],
  imports: [
    CommonModule,
    PageRoutingModule,
    themeModule.forRoot(),
    NgbModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    FacebookModule.forRoot()
  ],
 providers: [QuestionService],
  // bootstrap: [AppComponent]
})
export class PageModule { }
